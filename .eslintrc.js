module.exports = {
  settings: {
    react: {
      pragma: 'React',
      version: 'detect'
    }
  },
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'react-app',
    'react-app/jest',
    'airbnb-base',
    'plugin:react/recommended'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
    parser: 'babel-eslint'
  },
  plugins: [
    'react',
    '@typescript-eslint'
  ],
  rules: {
    'linebreak-style': 0,
    indent: [2, 4],
    camelcase: 2,
    'max-len': [2, { code: 200 }],
    'no-console': 0, // WebPack will remove it in production mode
    'import/extensions': 0, // WebPack and TypeScript already do this.
    'import/no-unresolved': 0, // WebPack and TypeScript already do this.
    'no-array-constructor': 0, // Allow to declare generic array like : new Array<ProctoringEventDocumentBase>();
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
}
