# mvp1-micro-frontend-react-app1

Simple listing of elements needed in Frontend

## Running Demo

Run `npm install` and `npm start` inside each repo respectively. This will build and serve your apps on ports 3001, 3002 and 3003

- [http://localhost:3101](http://localhost:3101)
- [http://localhost:3101/remoteEntry.js](http://localhost:3101/remoteEntry.js)