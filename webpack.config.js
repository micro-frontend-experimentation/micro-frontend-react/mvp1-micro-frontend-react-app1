const HtmlWebpackPlugin = require('html-webpack-plugin')
const { ModuleFederationPlugin } = require('webpack').container
const path = require('path')

module.exports = {
  entry: './src/index',
  mode: 'development',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    port: 3101
  },
  output: {
    publicPath: 'http://localhost:3101/'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: ['@babel/preset-react']
        }
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'app1',
      library: {
        type: 'var',
        name: 'app1'
      },
      filename: 'remoteEntry.js',
      exposes: {
        // expose each component
        './App': './src/App'
      },
      shared: {
        react: {
          version: '17.0.1',
          eager: true
        },
        'react-dom': {
          version: '17.0.1',
          eager: true
        }
      }
    }),
    new HtmlWebpackPlugin({
      template: './public/index.html'
    })
  ]
}
