import React from 'react'

export default function Card ({ buttonClick, title, buttonTitle }) {
  return (
    <div className='card'>
      <div className='header'>
        <h3>{title || 'Header'}</h3>
      </div>
      <div className='body'>
        <h3>Body</h3>
      </div>
      <div className='footer'>
        <h3>Footer</h3>
        <button onClick={buttonClick}>{buttonTitle || 'Button here'}</button>
      </div>
    </div>
  )
}
